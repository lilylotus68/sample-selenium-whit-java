package chapter01;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeMethod;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;

public class S0103_ByName_BayClass {
	WebDriver driver;
	@Test
	public void f() throws InterruptedException {
		WebElement searchBox = driver.findElement(By.name("q"));
		searchBox.sendKeys("Phones");
		WebElement searchButton = driver.findElement(By.className("search-button"));
		searchButton.click();
		Thread.sleep(2000);
		Assert.assertEquals(driver.getTitle(), "Search results for: 'Phones'");
		
	}
	@BeforeMethod
	public void beforeMethod() {
		System.setProperty("webdriver.chrome.driver", "./src/test/resources/drivers/chromedriver.exe");
		driver = new ChromeDriver();
		driver.get("http://demo-store.seleniumacademy.com/");
	}

	@AfterMethod
	public void afterMethod() {
		driver.quit();
	}

}
